use crate::interpreter::RuntimeError;
use crate::object::Object;
use std::time;
use std::time::UNIX_EPOCH;

pub trait Callable {
    fn call(&self) -> Result<Object, RuntimeError>;
    fn arity(&self) -> usize;
    fn box_clone(&self) -> Box<dyn Callable>;
}

impl Clone for Box<dyn Callable> {
    fn clone(&self) -> Box<dyn Callable> {
        self.box_clone()
    }
}

#[derive(Clone)]
pub struct Clock {}

impl Callable for Clock {
    fn arity(&self) -> usize {
        return 0;
    }

    fn call(&self) -> Result<Object, RuntimeError> {
        let now = time::SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
        Ok(Object::Number(now.as_secs() as f64))
    }

    fn box_clone(&self) -> Box<dyn Callable> {
        Box::new((*self).clone())
    }
}

impl std::fmt::Debug for Clock {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<native fn>")
    }
}

impl std::fmt::Debug for dyn Callable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<native fn>")
    }
}

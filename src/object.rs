use crate::callable::Callable;



#[derive(Debug, Clone)]
pub enum Object {
    Bool(bool),
    Number(f64),
    String(String),
    Nil,
    Callable(Box<Callable>)
}

impl PartialEq for Object {

fn eq(&self, other: &Self) -> bool {
    match (self, other) {
        (Object::Bool(a), Object::Bool(b)) => a.eq(b),
        (Object::Bool(false), Object::Number(0.0)) => true,
        (Object::Bool(false), Object::Number(_)) => false,
        (Object::Bool(false), Object::String(_)) => false,
        (Object::Bool(true), Object::Number(_)) => true,
        (Object::Bool(true), Object::String(_)) => true,
        (Object::Bool(true), Object::Callable(f)) => f.call().unwrap().eq(&Object::Bool(true)),
        (Object::Bool(false), Object::Callable(f)) => f.call().unwrap().eq(&Object::Bool(false)),

        (Object::Number(a), Object::Number(b)) => a.eq(b),
        (Object::Number(_), Object::String(_)) => false,
        (Object::Number(0.0), Object::Bool(false)) => true,
        (Object::Number(_), Object::Bool(true)) => false,
        (Object::Number(_), Object::Bool(false)) => false,
        (Object::Number(a), Object::Callable(f)) => f.call().unwrap().eq(&Object::Number(*a)),

        (Object::String(a), Object::String(b)) => a.eq(b),
        (Object::String(_), Object::Number(_)) => false,
        (Object::String(_), Object::Bool(true)) => true,
        (Object::String(_), Object::Bool(false)) => false,
        (Object::String(f), Object::Callable(g)) => Object::String(f.to_string()).eq(&g.call().unwrap()),

        (Object::Callable(f), Object::Callable(g)) => f.call().unwrap().eq(&g.call().unwrap()),
        (Object::Callable(f), Object::Bool(g)) => f.call().unwrap().eq(&Object::Bool(g.to_owned())),
        (Object::Callable(f), Object::Number(g)) => f.call().unwrap().eq(&Object::Number(g.to_owned())),
        (Object::Callable(f), Object::String(g)) => f.call().unwrap().eq(&Object::String(g.to_owned())),

        (Object::Nil, Object::Nil) => true,
        (_, Object::Nil) => false,
        (Object::Nil, _) => false,
        }
    }
}

impl Eq for Object {}

// impl Clone for Object {fn clone(&self) -> Self { todo!() }
// }
